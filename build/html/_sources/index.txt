. Sphinx, Webhooks, and Bitbucket documentation master file, created by
   sphinx-quickstart on Tue Mar  1 01:00:21 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
	:maxdepth: 2
	:numbered:
	:titlesonly:
	:glob:
	:hidden:
	
	Basic_reStructuredText.rst
	Converting_to_HTML.rst
	Style_Sheets.rst
	Webhooks_and_Automation.rst
	Advanced_Read_the_Docs.rst

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
	
#############################################################
Restructured Text for Building Webpages for Git Repositories
#############################################################

Part of the Data Intensive Biology Training program, UC Davis
http://dib-training.readthedocs.org/en/pub/

Instructor: Adelaide Rhodes, Ph.D., Oregon State University, Center for Genome Research and Biocomputing

***************
Description:
***************

This class will walk through the creation of webpages that can be linked through bitbucket or github as well as on your own personal server.  The first part of class will provide basic syntax for reStructured Text for developing webpages.  The second part of the class will use Sphinx for html conversion into indexable webpages with many options.  The third part of the class will discuss how to put the webpage up on readthedocs.org via webhooks from a bitbucket or github account.

********************
Learning Objective:
********************

* The ability to create webpages in a matter of minutes without a lot of confusing syntax
* The conversion of reST to webpage readable formats through an intermediary program such as sphinx
* The ability to link content to a live url via a github or bitbucket account 

***********************************
Webtools to Sign up For Beforehand
***********************************
* A github or bitbucket account (both free) will allow you to put your website up.  Github is more intuitive for beginners, bitbucket is prettier
* A readthedocs.org account (also free)
* An amazon account to access the EC2 instances (we will only be using free options)


**********************************************
Dependencies to Have Loaded on Your Computer
***********************************************

For the tutorial on reStructured Text, I recommend having a good text editor on your computer:

Windows:
=========

I recommend Notepad++ or Sublime3 for Windows. TextEdit is okay, in windows, but does not have the nice formatting options of Notepad++. Sublime3 automatically recognizes and colorcodes several programming languages.

Windows Text Editors:
======================

* https://notepad-plus-plus.org/download
* https://github.com/atom/atom/releases/download/v1.5.3/atom-windows.zip
* https://www.sublimetext.com/3

MacOSX:  
========
I recommend Atom or Text Wrangler for Mac.  Atom has the added function of being able to preview certain types of markdown (not reST) in an adjacent window and will allow direct edits to a github repository within the program.  Text Wrangler allows files to be saved in Unix-code directly, and has a spellchecker built in.

* https://s3.amazonaws.com/BBSW-download/TextWrangler_5.0.2.dmg
* https://atom.io/
* https://www.sublimetext.com/3

The most important feature is to have a text editing tool that does not add back-end coding (e.g. Microsoft Word) and that has line numbering along the side.  This will be important later when Sphinx builds the Makefile.

OPTIONAL DOWNLOADS
===================

We will be reinterpreting our reST into html using sphinx and/or pandocs. These tools can be downloaded to your laptop, but to facilitate the tutorial, we are actually going to open and run instances on Amazon EC2. Please read this information beforehand: http://angus.readthedocs.org/en/2015/amazon/index.html

In order to use the EC2 instance, it will be necessary to have a good windows terminal program installed, such as Putty.  Also, an scp tool such as WinScp would be helpful for data transfer between the EC2 and your laptop.

If you would prefer to work from your desktop, here are the download links for sphinx:

http://www.sphinx-doc.org/en/stable/install.html

Sphinx is a python module, so python will be installed as well.

Here are the instructions for pandocs **[NOTE:  THIS CAN BE A VERY COMPLEX INSTALL, IT IS NOT REQUIRED TO PARTICIPATE IN CLASS…., JUST SKIP THE INSTALL IF YOU GET STUCK]**

http://pandoc.org/installing.html

To proceed to the tutorial, follow this link:

.. :doc: `Basic_reStructuredText.rst

