
#########################
reStructuredText Tutorial
#########################

**************
Introduction
**************

reStructuredText is a simplified mark-up language. These simplified patterns are interpreted by a HTML converter to produce "Very Structured Text" read by a web browser. I started using reStructuredText after forgetting one too many </p></a></strong> in my html files.  I would get overwhelmed by the multiple lines of code and would give up in frustration after a few minutes of trying to keep my eyes uncrossed.

In reST, there are only a few rules to remember, and most of them hinge upon the special symbols that come before and after regular text. I usually start by writing a simple piece of text, then adding in the symbols from top to bottom. You may like to add the symbols as you go.

**********************
Start of the Tutorial
**********************

Before beginning, take a few minutes to download a good text editor for your computer if you do not already have one.

I recommend Notepad++ or Sublime3 for Windows. TextEdit is okay, in windows, but does not have the nice formatting options of Notepad++. Sublime3 automatically recognizes and colorcodes several programming languages.

I recommend Atom or Text Wrangler for Mac.  Atom has the added function of being able to preview certain types of markdown (not reST) in an adjacent window and will allow direct edits to a github repository within the program.  Text Wrangler allows files to be saved in Unix-code directly, and has a spellchecker built in.

The most important feature is to have a text editing tool that does not add back-end coding (e.g. Microsoft Word) and that has line numbering along the side.  This will be important later when Sphinx builds the Makefile.

If you do not have a text editing tool installed, please do so now.  Fastest installs are Notepad++ for windows and Text Wrangler for Mac.

Windows Text Editors:

* https://notepad-plus-plus.org/download
* https://github.com/atom/atom/releases/download/v1.5.3/atom-windows.zip
* https://www.sublimetext.com/3

Mac Text Editors:

* https://s3.amazonaws.com/BBSW-download/TextWrangler_5.0.2.dmg
* https://atom.io/
* https://www.sublimetext.com/3

Paragraph
==========

The basic building block of reST is the paragraph.

A paragraph is a set of text that is separated by blank lines, one line is enough.

	A paragraph that is indented in reST will be indented on the web page.
	
Paragraphs that need to be aligned should start in the same location (in this case all the way to the left).

Inline markup
==============

Symbols used in resStructeredText (reST) asterisk "*","backslash "\",grave accent "`" ::

	Simplest case: inline markup with *italics* and **bold**


Simplest case: inline markup with *italics* and **bold**

	Yes, it is that simple.

If the "*" is inside a line, say an equation 6*30=180, reST will ignore
it. If the * is all by itself, surrounded by spaces, reST will ignore
it.

What if you have a line you want to start with "*", but not be italicized?

First method:  
---------------

Place a backslash in front of the symbol. ::

	Now the html interpreter will see the \*word* and ignore the italics symbol.

Now the html interpreter will see the \*word* and ignore the italics symbol.

This is similar to the "escape" function that \ is used for in regex, or when you are transferring a file with spaces in the name. \ ignores the "specialness" of the character.

Second method:
---------------

That is where use a new symbol:

"`" = grave accent, sometimes referred to as "backtick" or "back quote" In Mac keyboards, this accent is found underneath the ~, to the left of number 1.

Here is a guide for finding the grave in your computer:

http://superuser.com/questions/254076/how-do-i-type-the-tick-and-backtick-characters-on-windows

What about if the word is not inline, but at the beginning of a paragraph?

To ignore the asterisk in the following condition - place the phrase within double grave accents. ::

``*word*`` becomes

``*word*``

instead of *word*

This method is called "inline literals", and I prefer to use this method, because it is more symmetrical to me. However, if you are using a lot of regular expression in your work, backslash may be more familiar to your style.

Some issues::

	reST may not be nested content may not start or end with whitespace: 
		* text* is wrong, 	
		A backslash escaped space to work around that: thisis\ *one*\ word.
		Backslash will not work at the beginning of a paragraph, list item or section in some html interpreters.

Lists
==============

Three kinds::

	1. enumerated, 
	*  bulleted, and 
	definitions
		indented text to define a term
		
If the left-hand side of the paragraph or aligns, you can have as many sublists as you want.

Lists must always start a new paragraph -- that is, they must appear after a blank line.

enumerated lists
------------------

How to do it:  Start a line off with a number or letter followed by a period ".", right parenthesis ")" or surrounded by parentheses "( )" 

1. numbers

A. upper-case letters.  Information can be continued over several lines, to create a new paragraph, make sure to leave a blank line.

   This is my second paragraph.

a. lower-case letters

   3. here is a sub-list starting at a different number
   4. it is up to you to check the sequence

I. upper-case roman numerals

i. lower-case roman numerals

(1) numbers in parentheses

1) numbers with one right parentheses

Known issue: the different enumerated list styles are not always supported by every web browser.

bulleted lists
-----------------

How to do it:  Just like enumerated lists, start the line off with a bullet point character "-", "+" or "*" :

* bullet point using "*"

  - sub-list using "-"

    + another sub-list using "+"

  - another item

* bullet point using "*"

  - sub-list using "-"

    + another sub-list using "+"

  - another item
  
Note:  nested lists require blank lines before the beginning of the sublist

definition lists
------------------

definition lists consist of a term, and the definition of that term. The format of a definition list is:

what
  Definition lists associate a term with a definition.

*how*
  The term is a one-line phrase.  The definition is one or more
  paragraphs or body elements, indented relative to the term.
  No blank lines between term and definition.

Pre-formatted text
====================

If I have something written out in a text document and I want to have it look exactly that way in reST, I can indicate that the text is "preformatted" by adding "::" to the end of the prior paragraph.

This is called a "literal block".
It only works if an indentation is provided for the text following the "::".
The block will appear as a slightly different color on the page, depending on which format you choose for Sphinx.

An example::

    Whitespace, newlines, blank lines, and **markup**
      (like *this* or \this) is preserved by literal blocks.
  Your mistakes are retained as well, (note poor alignment here)

no more example

To hide the :: from the html version, just add a whitespace before the ``::``

If you are putting the pre-formatted text at the beginning of a section and there is no paragraph beforehand, you can just put "::" at the beginning of a line.  Make sure to skip a line between that :: and the start of your pre-formatted text.::

                                    _
                                 ,:'/   _..._
                                // ( `""-.._.'
                                \| /    6\___
                                |     6      4
                                |            /
                                \_       .--'
                                (_'---'`)
                                / `'---`()
                              ,'        |
              ,            .'`          |
              )\       _.-'             ;
             / |    .'`   _            /
           /` /   .'       '.        , |
          /  /   /           \   ;   | |
          |  \  |            |  .|   | |
           \  `"|           /.-' |   | |
            '-..-\       _.;.._  |   |.;-.
                  \    <`.._  )) |  .;-. ))
                  (__.  `  ))-'  \_    ))'
                      `'--"` Rambo `"""`

Section Headers
==================

You might have noticed that I have been interlacing my sections with non-alpha-numeric characters as I write this document.

* Sections can be indicated by the following: = - ` : ' " ~ ^ _ * + # < >
* The underline symbol must extend as long as the text, and appear immediately underneath the name of the section
* Follow the underline symbol with a blank line
* Sections with the same symbol will be considered to be the same level in the text, so it is important to remember which symbols you used for each level.

So far, we have the following set of sections laid out. ::

 ****************************
 Introduction - 
 ****************************

 ********************************************
 Start of the Tutorial -
 ********************************************

 Paragraph - Section 
 ===============================

 Inline markup - Section 
 ===================================

 First method: - Subsection  
 ------------------------------------

 Second method: - Subsection
 -------------------------------------

This layout provides header text that looks like this:

****************************
Introduction - "Chapter I"
****************************

********************************************
Start of the Tutorial - "Chapter II
********************************************

Paragraph - Section 
===============================

Inline markup - Section 
===================================

First method: - Subsection  
------------------------------------

Second method: - Subsection
-------------------------------------


So, there is some method to this madness......

Another cool feature of section headers is that you can refer back to the link.  You may have noticed that I just changed the names of my sections slightly, because each section has to have a unique name.  This is generally not a problem when you are organizing a science paper or setting up a class website.  Dates are a good way to avoid repeating header names.  Sequential numbering of sections is another.

To link to a section or chapter in your document is very simple: ::

	+ I can return to the "Introduction_"
	+ Or, if there is a space in the name of the header, I can use the grave accent inside the quotes, for example "`Section Headers`"
	+ You might want to be sparing in the number of headers, because they end up in the table of contents.  Too many headers can clutter up the space.

+ I can return to the "Introduction_"
+ Or, if there is a space in the name of the header, I can use the grave accent inside the quotes, for example "`Section Headers`"
+ You might want to be sparing in the number of headers, because they end up in the table of contents.  Too many headers can clutter up the space.

Document titles and subtitles
===============================

You may have noticed that my section list did not included 

::

	###########################
	reStructuredText Tutorial
	###########################

That is because the overline/underline style indicates a document title.  This is shown by the html interpreter as a centered heading on your page.  It is important to know that in some html interpreters, the overline/underline is interpreted as a document "title", and would be centered in the page.  We are using Sphinx, which has it's own definition of headers/titles, so it is best to stick to the conventions laid out below to get your reST file converted neatly into html using Sphinx.

Again, you can create titles and subtitles using the same characters as headers use, the main difference is that titles will be centered on the page and headers will be left-aligned [IN CERTAIN WEB BROWSERS --- BUT NOT A SPHINX CREATED WEBPAGE WHICH WE ARE CONSTRUCTING AFTER THE BREAK].

Some web browsers will interpret headers in a certain priority, so if you want to stick to these conventions, you should be pretty safe, especially because we are using Sphynx to reinterpret our reST into html in the next part:

::

	# with overline, for parts
	* with overline, for chapters
	=, for sections
	-, for subsections
	^, for subsubsections
	", for paragraphs

Normally, there are no heading levels assigned to certain characters as the structure is determined from the succession of headings, but sticking to this convention will ensure that the web interpreter constructs it correctly.


Copy and Paste your page into the following website to check your syntax:
==========================================================================

http://rst.ninjs.org/

or, a cool tool that can be added to your python scripts is:

https://launchpad.net/restview

Correct any errors that you can see.
=====================================

Move on to Part II
=====================

Please follow this link.

:doc:`Converting_to_HTML`


*********************
Additional Readings
*********************

* http://www.sphinx-doc.org/en/stable/rest.html
* http://docutils.sourceforge.net/docs/user/rst/quickstart.html
* http://www.florian-diesch.de/doc/sphinx/themes-and-pygments-styles/index.html
* http://www.adamhyde.net/whats-wrong-with-markdown/
* http://evolvingweb.ca/story/writing-documentation-restructured-text-and-sphinx
* https://github.com/brandon-rhodes/sphinx-tutorial
* http://tjelvarolsson.com/blog/how-to-generate-beautiful-technical-documentation/
* https://pythonhosted.org/an_example_pypi_project/sphinx.html?highlight=release%20variable
* https://github.com/spatialaudio/nbsphinx/tree/0.2.4
* http://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html#include-other-rst-files-with-the-toctree-directive

