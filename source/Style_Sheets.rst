********************************
MAKING OUR DOCUMENTS LOOK NICE
********************************

Right now, our style is a little bland.

Sphinx on the Mac will add a default style called "alabaster".

Sphinx on our ubuntu install left it as basic.

This is easy to fix by providing a few more style sheets and changing our config file.

Working from the EC2 instance, the following files are required: ::

	sudo apt-get install python-setuptools python-dev build-essential
	sudo easy_install pip
	sudo pip install sphinxjp.themes.basicstrap
	sudo pip install sphinx_bootstrap_theme

Working from your Mac or Windows, these files can be downloaded through git.  When sphinx is installed, it automatically updates python.

Sphinx install directions: http://www.sphinx-doc.org/en/stable/install.html#mac-os-x-install-sphinx-using-macports
Sphinxjp.themes.basicstrap: https://github.com/tell-k/sphinxjp.themes.basicstrap
Sphinx_bootstrap_theme: https://pypi.python.org/pypi/sphinx-bootstrap-theme/

Now we need to change our theme to basic strap and choose from one of the options available on this website:

https://pythonhosted.org/sphinxjp.themes.basicstrap/index.html

It has a very nice interactive viewer so you can decide which one you prefer.

I have chosen the "superhero" theme, so my websites can have superpowers.  You can substitute "superhero" below with your preferred theme name from bootswatch.

***************************************************************
Changing the Theme on the Local Computer or Remotely on the EC2
***************************************************************

Choose this option if you want to create html pages on your computer for upload to a url that you own, or to preview the look of a page once it gets transferred to readthedocs.org via githbub or bitbucket. ::

	nano ~/Tutorial/source/conf.py

Find the **html_extensions** line and replace the one line with two: ::

	#html_theme = 'default'
	extensions = ['sphinxjp.themes.basicstrap']
	html_theme = 'basicstrap'
	
Scroll down to the next paragraph, uncomment the **html_theme_options** and put the desired style in to the text. ::

	#html_theme_options = {}
	html_theme_options = {
	  'inner_theme': True,
	  'inner_theme_name': 'bootswatch-superhero',
	}


Let's rebuild our webpage and take a look at it now. ::

	cd ~/Tutorial
	make html


Open your local html file or bring your new html to the desktop by scp from the remote EC2 instance.  This has to be done from your **local** terminal to work.  Open a new terminal window separate from your EC2 instance and do not log into the EC2 instance before running this command.  This is easier to do on a Windows machine if you have OpenSSH or another terminal program already installed.

Example for scp: ::

	scp -i ~/Desktop/reST.pem ubuntu@ec2-54-166-128-20.compute-1.amazonaws.com:/Tutorial/* ~/Desktop/

You will need to specify the location of your .pem file and your instance address.

-------------------------------------------
The next section deals with how to create an automatic webhook on bitbucket and github.  We can bring our style with us!

:doc:`Webhooks_and_Automation`

