********************************
OPEN an EC-2 INSTANCE on Amazon
********************************

All the work we are doing can be done on a laptop.  It just requires installation of the sphinx-doc program and docutils to get started:

http://www.sphinx-doc.org/en/stable/install.html]

Since some students may not have the ability to load the latest versions of these programs, it is possible to do a clean install using the EC2 instance at Amazon Web Services.

http://angus.readthedocs.org/en/2015/amazon/index.html

Open an instance that is Ubuntu, and pick the free machine option (we are not loading very large files for this tutorial).

**Remember to terminate your instance at the end of the tutorial.**


*********************
INSTALL DEPENDENCIES:
*********************
 ::
 
	sudo apt-get install ghc6 libghc6-xhtml-dev libghc6-mtl-dev libghc6-network-dev
	sudo apt-get install make
	sudo apt-get install docutils
	sudo apt-get install python-sphinx	

	wget https://github.com/yihui/ubuntu-bin/releases/download/latest/texlive.tar.gz
	tar -xvf texlive.tar.gz

Let's make a directory for our project. ::

	mkdir ~/Tutorial

[Sphinx is relatively easy to download and install on Mac and PC, here are the instructions:

http://www.sphinx-doc.org/en/stable/install.html]

For the purpose of today's tutorial, we have already installed all the programs we need on our instance.  The directions from here forward are very similar, except the extra step of transferring your html to your desktop if you are using the instance to build an html.

In order to convert our reStructred text document to html, we are going to transfer our files to the instance, or else install all the required programs to the desktop.  S

From a terminal not inside your EC2 instance, you can type this at the command line: ::

	scp -i ~/Desktop/reST.pem ~/Desktop/Tutorial.rst  ubuntu@ec2-54-166-128-20.compute-1.amazonaws.com:~/Tutorial


Note:  make sure to do this from a terminal that is not in the EC2 instance, it's relative path is based on your laptop.

Change the .pem file to the name and location of your file.
Change the instance address to the instance in your address.

This may not work if you are on a Windows machine, but you can use a file transfer program, as detailed here:

http://angus.readthedocs.org/en/2014/amazon/transfer-files-between-instance.html

The reverse can be accomplished if you need to transfer the files back to your desktop for the bitbucket/github drag and drop step of the tutorial.  If scp did not work for your previously, please use the ftp feature. ::

	scp -i ~/Desktop/reST.pem ubuntu@ec2-54-166-128-20.compute-1.amazonaws.com:/Tutorial/* ~/Desktop/

******************************
Running Sphinx the First Time
******************************

Since this is our first time creating a document using sphinx, let's use the simplest option possible.

 ::
 
	cd Tutorial
	~sphinx-quickstart


This is what you will see printed on the screen.  Answer the questions as below.
Hit Control +C if you make a mistake.
--------------------------------------------------::

	Welcome to the Sphinx 1.3.1 quickstart utility.

	Please enter values for the following settings (just press Enter to
	accept a default value, if one is given in brackets).

	Enter the root path for documentation.
	> Root path for the documentation [.]

Press Enter to stay in this directory - the easiest thing to do is create a separate directory for each html page, place the .rst file you wish to convert in that directory and then build from sphinx-quickstart inside that same directory.  Once you get good at the process, you may want to designate a base directory for all of your html pages that contains all of your work ::

	You have two options for placing the build directory for Sphinx output.
	Either, you use a directory "_build" within the root path, or you separate
	"source" and "build" directories within the root path.
	> Separate source and build directories (y/n) [n]: y

Answer y ::

	Inside the root directory, two more directories will be created; "_templates"
	for custom HTML templates and "_static" for custom stylesheets and other static
	files. You can enter another prefix (such as ".") to replace the underscore.
	> Name prefix for templates and static dir [_]:

We are not setting this feature yet, so hit enter. ::

	The project name will occur in several places in the built documentation.
	> Project name: Sphinx and reST Tutorial
	> Author name(s): Adelaide Rhodes, Ph.D.
	
Replace the project name and author name with your preferences. ::

	Sphinx has the notion of a "version" and a "release" for the
	software. Each version can have multiple releases. For example, for
	Python the version is something like 2.5 or 3.0, while the release is
	something like 2.5.1 or 3.0a1.  If you don't need this dual structure,
	just set both to the same value.
	> Project version: 1.0
	> Project release [1.0]: 1.0
	
Since this is the first time setting up our project, it is good to stick with the naming convention of "1.0" for both options. ::
	
	If the documents are to be written in a language other than English,
	you can select a language here by its language code. Sphinx will then
	translate text that it generates into that language.

	For a list of supported codes, see
	http://sphinx-doc.org/config.html#confval-language.
	> Project language [en]: en::
	
Default is English. ::

	The file name suffix for source files. Commonly, this is either ".txt"
	or ".rst".  Only files with this suffix are considered documents.
	> Source file suffix [.rst]: .rst

We are using .rst as the option.  .rst = reStructured text format. ::

	One document is special in that it is considered the top node of the
	"contents tree", that is, it is the root of the hierarchical structure
	of the documents. Normally, this is "index", but if your "index"
	document is a custom template, you can also set this to another filename.
	> Name of your master document (without suffix) [index]: Tutorial
	

You can pick a different name if you like.  Don't put .rst on the end, or it will make a file that looks like: ::

	Tutorial.rst.rst

::

	Sphinx can also add configuration for epub output:
	> Do you want to use the epub builder (y/n) [n]: y

The epub builder is useful when transferring to the readthedocs.org format. ::

	Please indicate if you want to use one of the following Sphinx extensions:
	> autodoc: automatically insert docstrings from modules (y/n) [n]: y
	> doctest: automatically test code snippets in doctest blocks (y/n) [n]: y
	> intersphinx: link between Sphinx documentation of different projects (y/n) [n]: y
	> todo: write "todo" entries that can be shown or hidden on build (y/n) [n]: y
	> coverage: checks for documentation coverage (y/n) [n]: y
	> pngmath: include math, rendered as PNG images (y/n) [n]: y
	> mathjax: include math, rendered in the browser by MathJax (y/n) [n]: y
	Note: pngmath and mathjax cannot be enabled at the same time.
	pngmath has been deselected.
	> ifconfig: conditional inclusion of content based on config values (y/n) [n]: y
	> viewcode: include links to the source code of documented Python objects (y/n) [n]: y

Please answer yes to all of the questions, except pick either pngmath or mathjax for your math rendering program.  Both cannot be enabled at the same time. ::

	A Makefile and a Windows command file can be generated for you so that you
	only have to run e.g. `make html' instead of invoking sphinx-build
	directly.
	> Create Makefile? (y/n) [y]: y
	> Create Windows command file? (y/n) [y]: y

Answer yes. ::

	Creating file ./source/conf.py.
	Creating file ./source/Tutorial Master.rst.
	Creating file ./Makefile.
	Creating file ./make.bat.

	Finished: An initial directory structure has been created.

You should now populate your master file ./source/Tutorial Master.rst and create other documentation source files. Use the Makefile to build the docs.  More than one building option is available, we are only using the html version for this tutorial.

Once the initial set of files has been constructed, sphinx can be triggered to rebuild the html file by typing: ::

	cd Tutorial
	make html


This allows us to automate any changes we make in our .rst file.

Let's investigate the build --- it won't have any content yet. ::

	cd ~/Tutorial/build
	ls
	
This is where our html file is located. ::

	cd ~/Tutorial/source
	ls

This is where our reST file is located. ::

	nano Tutorial.rst
	

Sphinx places default text into the top of the file.  You would want to keep the following parts: ::

	.. Intro documentation master file, created by
   sphinx-quickstart on Mon Feb 29 15:24:58 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
	.. toctree::
	   :maxdepth: 2
	   
	* :ref:`search`

This will build a search object and an index object for your website.  Two useful tools once the website is published.  This will create a table of contents that descends the top two levels.  maxdepth: 3 would also work in our case.  ``* :ref: search`` makes a search box in the html.  This is helpful for very long files.

To add content to the Tutorial.rst file, you have two options. The easiest way to add content is to add the toctree and refsearch commands from above to the top of your existing file and save it in the source directory.  Next time you run the make file, it will see all the content you have added to the .rst file in the source directory and generate an html version in the build/html/ directory.

Or, if you have not written your content yet, open the .rst file in the source directory and start adding content after the default advanced directives made by the initial build.  Be aware that any text or directive you do not erase will be visible in the html file.

Once you have added some content to the .rst file in the source directory, we are ready to rebuild. ::

	cd ~/Tutorial
	make html

NOTE:  You may get errors, this why it is helpful to have a line counting text editor on your desktop.

In order to see the result of our project, let's bring the files to our desktop and open them up in a web browser. ::


	scp -i ~/Desktop/reST.pem ubuntu@ec2-52-90-11-137.compute-1.amazonaws.com:~/Tutorial/build/html/Tutorial.html ~/Desktop/


Note:  make sure to do this from a terminal that is not in the EC2 instance, it's relative path is based on your laptop.

Change the .pem file to the name and location of your file.
Change the instance address to the instance in your address.

This may not work if you are on a Windows machine, but you can use a file transfer program, as detailed here:

http://angus.readthedocs.org/en/2014/amazon/transfer-files-between-instance.html

Two good file transfer programs are the WinSCP for Windows and Cyberduck for Mac.

********************
Open your HTML file
********************

It is not very exciting... yet.  If you built on the Mac version of sphinx, it may have the alabaster theme included in the initial build.

However, once you have successfully created a format that you like, you can add new pages to your project by simply adding them to the source directory where your first .rst file was located.  It is a good idea to have an index.rst file at some point, that will be useful for creating automated webpages using the readthedocs.org tool later on.  Another good file to create is a simple README.rst file that describes the project and will serve as your frontpage on github.

The next phase is to spruce it up with style sheets, etc.

:doc:`Style_Sheets`